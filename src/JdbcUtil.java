import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class JdbcUtil {
 
	private static JdbcUtil jdbcutil_s = null;
	
	private JdbcUtil() {}
	
	public static synchronized JdbcUtil getJdbcUtil() {
		if(jdbcutil_s == null) {
			jdbcutil_s = new JdbcUtil();
		}
		return jdbcutil_s;
	}
	
	public static Connection getConnection(){
	  Connection con=null;
	  try{
	   Class.forName("com.mysql.jdbc.Driver");
	   con = DriverManager.getConnection("jdbc:mysql://localhost:3306/email_db", "root", "anusha123");
	  }catch(Exception e){
	   e.printStackTrace();
	  }
	  return con;
  }
	
   public synchronized ArrayList<Email> fetchEmails(int limit){
	   ArrayList<Email> emails = new ArrayList<Email>();
	   Connection con = getConnection();
	   String query;
	   try{
		 Statement stmt = con.createStatement();
		 String status = "fresh";
		 ResultSet rs;
		 query = "select * from email_queue WHERE process_status ='"+status+"' and fetched = '0' LIMIT " + limit;
		 rs = stmt.executeQuery(query);
		 while(rs.next()){
			 int id = Integer.parseInt(rs.getString(1));
			 String fromEmailAddress = rs.getString(2);
			 String toEmailAddress = rs.getString(3);
			 String subject = rs.getString(4);
			 String body = rs.getString(5);
			 String processStatus = rs.getString(6);
			 emails.add(new Email(id, fromEmailAddress, toEmailAddress, subject, body, processStatus));
			 setFetchedToTrue(id);
		 }
	
		 }catch(SQLException e){
			 e.printStackTrace();
		 }
		 finally{
			 try{
			 query = "COMMIT";
			 Statement stmt = con.createStatement();
			 stmt.executeQuery(query);
			 con.close();
			 }catch(SQLException e){
			 e.printStackTrace();
			 }
		 }
	   return emails;
   }
   
   private boolean setFetchedToTrue(int email_id) {
	   Connection con = getConnection();
	   try{
		 Statement stmt = con.createStatement();
		 String status = "1";
		 String query = "update email_queue set fetched = '"+status+"' WHERE id = " + email_id;
		 int rows = stmt.executeUpdate(query);
		 if(rows < 0){
			 return false;
		 }
	   }catch(SQLException sqle){
		 sqle.printStackTrace();
	   }
	   finally{
		 try {
			con.close();
		 } catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		 }
	   }
	   return true;
   }
   
   public void updateEmailStatus(Email e){
	   Connection con = getConnection();
	   try{
		 Statement stmt = con.createStatement();
		 String status = e.getProcessStatus();
		 String query = "update email_queue set process_status = '"+status+"' WHERE id = " + e.getId();
		 stmt.executeUpdate(query);
	   }catch(SQLException sqle){
		 sqle.printStackTrace();
	   }
	   finally{
		 try {
			con.close();
		 } catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		 }
	   }
   }
   
   public int updateFailedMais(){
	   int rows = 0;
	   Connection con = getConnection();
	   try{
		 Statement stmt = con.createStatement();
		 String query = "update email_queue set process_status = 'fail' WHERE process_status = 'fresh' and fetched = '1'";
		 rows = stmt.executeUpdate(query);
	   }catch(SQLException sqle){
			 sqle.printStackTrace();
	   }
	   finally{
		 try {
			con.close();
		 } catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		 }
	   }
	   return rows;
   }
}