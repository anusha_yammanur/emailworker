import java.util.ArrayList;


public class InitializeEmailSender {	
	public static void main(String[] args) {
		System.out.println("Initialize Email Sender... ");
		ArrayList<Email> emails = JdbcUtil.getJdbcUtil().fetchEmails(300);
		EmailList el = EmailList.getEmailList();
		el.addEmails(emails);
		MonitorWorkers mw = new MonitorWorkers();
		mw.startMonitorWorkers();
		JdbcUtil.getJdbcUtil().updateFailedMais();
	}
}
