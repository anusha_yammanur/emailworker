import java.util.ArrayList;

public class DBWorker extends Thread {
	
	public void startDBWorker() {
		DBWorker dbw = new DBWorker();
		dbw.start();
	}
	
	public void run(){
		while(EmailWorkerCounter.getEmailWorkerCounter().getEWorkerCounter() > 0 || !DBWorkerQueue.getDBWorkerQueue().isQueueEmpty() ){
			updateEmailStatus();			
		}
	}
	
	private void updateEmailStatus(){
		Email e = DBWorkerQueue.getDBWorkerQueue().deQueueEmail();
		if(e != null) {
			System.out.println("Updating process_status of email_id " + e.getId() + " in database.");
			JdbcUtil.getJdbcUtil().updateEmailStatus(e);
			updateEmailList();
		}		
	}

	private void updateEmailList() {
		ArrayList<Email> emails = JdbcUtil.getJdbcUtil().fetchEmails(20);		
		if(emails.size() > 0){			
			for(int i=0; i<emails.size(); i++ ){				
				EmailList.getEmailList().enQueueEmail(emails.get(i));
				EmailWorkerCounter.getEmailWorkerCounter().decrementWorkerCounter();
			}
		}
	}
}
