import java.util.LinkedList;
import java.util.Queue;


public class DBWorkerQueue {
	private static DBWorkerQueue db_worker_queue_s = null;
	
	private Queue<Email> dbWorkerQueue = new LinkedList<Email>();
	
	private DBWorkerQueue() {}
	
	public synchronized static void initDBWorkerQueue() {
		if(db_worker_queue_s == null) {
			db_worker_queue_s = new DBWorkerQueue();
		}
	}
	
	public synchronized static DBWorkerQueue getDBWorkerQueue() {
		if(db_worker_queue_s == null) {
			db_worker_queue_s = new DBWorkerQueue();
		}
		return db_worker_queue_s;
	}
	
	public synchronized boolean isQueueEmpty() {
		return dbWorkerQueue.isEmpty();
	}
	
	public synchronized void enQueueEmail(Email email) {
		dbWorkerQueue.add(email);
	}
	
	public synchronized Email deQueueEmail() {
		if(!dbWorkerQueue.isEmpty()) {
			try{
				return dbWorkerQueue.remove();
			}catch(java.util.NoSuchElementException e){
				dbWorkerQueue.clear();
				EmailWorkerCounter.getEmailWorkerCounter().setEWorkerCounter(0);
				return null;
			}
		}
		else {
			return null;
		}
	}
}
