import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.ArrayList;



public class EmailWorker extends Thread {
	
	public void startEmailWorker() {
		EmailWorker worker = new EmailWorker();
		worker.start();
	}
	
	public void run() {
		this.sendEmails();
	}
	
	public void sendEmails() {
		ArrayList<Email> emails = EmailList.getEmailList().getEmailsInBatch(1);
		for(int i=0; i<emails.size(); i++){
			Email e = emails.get(i);
			if(e != null){
				String to = e.getToEmailAddress();
				
				// Sender's email ID needs to be mentioned
				String from = e.getFromEmailAddress();
				
				// Assuming you are sending email from localhost
				String host = "localhost";
				
				// Get system properties
				Properties properties = System.getProperties();
				
				// Setup mail server
				properties.setProperty("mail.smtp.host", host);
				
				// Get the default Session object.
				Session session = Session.getDefaultInstance(properties);
				
				try{
					// Create a default MimeMessage object.
					MimeMessage message = new MimeMessage(session);
				
					// Set From: header field of the header.
					message.setFrom(new InternetAddress(from));
				
					System.out.println("To address: "+to);
					// Set To: header field of the header.
					message.addRecipient(Message.RecipientType.TO,
				                          new InternetAddress(to));
				
					// Set Subject: header field
					message.setSubject(e.getSubject());
				
					// Now set the actual message
					message.setText(e.getBody());
				
					// Send message
					Transport.send(message);
					System.out.println("Sent message successfully....");
					e.setProcessStatus("success");
					//DBWorkerQueue.getDBWorkerQueue().enQueueEmail(e);
				}catch (MessagingException mex) {
					e.setProcessStatus("fail");
					//DBWorkerQueue.getDBWorkerQueue().enQueueEmail(e);
				} finally {				
					DBWorkerQueue.getDBWorkerQueue().enQueueEmail(e);
				}
			}
		}
	}
}
