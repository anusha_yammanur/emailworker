
public class MonitorWorkers extends Thread{
	
	public void startMonitorWorkers() {
		MonitorWorkers workers = new MonitorWorkers();
		workers.start();
	}
	
	public void run() {
		while(!EmailList.getEmailList().isEmailListEmpty() || !EmailWorkerCounter.getEmailWorkerCounter().noWorkersRunning()){
			if( !EmailList.getEmailList().isEmailListEmpty()) {
				if( EmailWorkerCounter.getEmailWorkerCounter().canCreateEmailWorker() ) {
					EmailWorker e_worker = new EmailWorker();
					e_worker.startEmailWorker();
					EmailWorkerCounter.getEmailWorkerCounter().incrementWorkerCounter();
					DBWorker dbw = new DBWorker();
					dbw.startDBWorker();
				}
			}
		}
	}
}
