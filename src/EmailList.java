import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;


public class EmailList {
	
	private static EmailList emailList_s = null;
	
	private Queue<Email> emailQueue = new LinkedList<Email>();
	
	private EmailList() {}
	
	public static synchronized EmailList getEmailList() {
		if(emailList_s == null) {
			emailList_s = new EmailList();
		}
		return emailList_s;
	}
	
	public synchronized void addEmails(ArrayList<Email> emails) {
		for(int i=0; i<emails.size(); i++) {
			Email email = emails.get(i);
			emailQueue.add( email);
		}
	}
	
	public synchronized void enQueueEmail(Email email) {
		emailQueue.add(email);
	}
	
	public synchronized ArrayList<Email> getEmailsInBatch(int n){
		ArrayList<Email> emails = new ArrayList<Email>();
		for(int i=0; i<n; i++){
			emails.add(deQueueEmail());
		}
		return emails;
	}
	
	public synchronized Email deQueueEmail() {
		try{
			return emailQueue.remove();
		}catch(Exception e){
			return null;
		}
	}
	
	public synchronized boolean isEmailListEmpty(){
		return emailQueue.isEmpty();
	}
	
}
