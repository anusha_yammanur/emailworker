public class EmailWorkerCounter {
	private static EmailWorkerCounter emailWorkerCounter_s = null;
	
	private static int counter = 1;
	
	private final int MAX_WORKERS = 300;
	
	private EmailWorkerCounter() {}
	
	public static synchronized EmailWorkerCounter getEmailWorkerCounter() {
		if(emailWorkerCounter_s == null) {
			emailWorkerCounter_s = new EmailWorkerCounter();
		}
		return emailWorkerCounter_s;
	}
	
	public boolean canCreateEmailWorker() {
		return counter <= MAX_WORKERS ? true : false;
	}
	
	public void incrementWorkerCounter() {
		counter = counter + 1;
	}
	
	public void decrementWorkerCounter() {
		counter = counter - 1;
	}
	
	public int getEWorkerCounter() {
		return counter;
	}
	
	public void setEWorkerCounter(int count) {
		counter = 0;
	}
	
	public boolean noWorkersRunning() {
		return counter == 0 ? true : false;
	}
}
